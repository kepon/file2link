<?php 

define('VERSION', '1.2');
if (!is_readable('./config.yaml')) {
    exit('Error: The configuration file is not present, move config.yaml.default to config.yaml');
}
if (($config = yaml_parse_file('./config.yaml')) == false) {
    exit('config.yaml syntax error, check with : http://www.yamllint.com/');
} 
include('./lib/functions.php');

// Init class
if ($config['check_checksum'] === true) {
    $checksum = new Checksum;
}

if ($config['authImap'] ==  true || $config['authGuest'] ==  true) {
    $auth = new Auth();
    // Post Auth login
    if (isset($_POST['loginForm'])) {
        if (isset($_POST['imapPassword']) && isset($_POST['imapLogin'])
            && $_POST['imapPassword'] != ''&& $_POST['imapLogin'] != '') {
            $login=$_POST['imapLogin'];
            $password=$_POST['imapPassword'];
            $type='imap';
        }
        if (isset($_POST['guestPassword']) && $_POST['guestPassword'] != '') {
            $login='guest';
            $password=$_POST['guestPassword'];
            $type='guest';
        }
        $authReturn = $auth->login($login, $password, $type, $_POST['dateExpire']);
        if (isset($authReturn['token'])) {
            setcookie("token",$authReturn['token'], time()+$_POST['dateExpire']*60*60, '/');
            header("Location: ".$_SERVER['SCRIPT_URL']."#login");
            exit();
        }
    }
    if (isset($_GET['log']) && $_GET['log'] == 'out') {
        $auth->logout($_COOKIE['token']);
    }
}

if (isset($_GET['id'])){
    $id = $_GET['id'];
    if (!preg_match('/^[0-9]+-[0-9]{1,2}$/', $id)) {
        exit('No Hack ID');
    }
} else {
    $id = null;
}


/* Language */
if (isset($_GET['langueChange'])) {
    $locale = lang2locale($_GET['langueChange']);
	$localeshort=locale2lang($locale);
    setcookie("langue",$localeshort,strtotime( '+1 year' ), '/');
} else {
    if (isset($_COOKIE['langue'])) {
        $locale = lang2locale($_COOKIE['langue']);
        $localeshort=locale2lang($locale);
    } else {
        $HTTP_ACCEPT_LANGUAGE=$_SERVER['HTTP_ACCEPT_LANGUAGE'];
        //echo $HTTP_ACCEPT_LANGUAGE.'<br />';
        $lang_from_http_accept = explode(',', $HTTP_ACCEPT_LANGUAGE);
        //echo $lang_from_http_accept[0].'<br />';
        $locale = lang2locale($lang_from_http_accept[0]);
        if (substr($locale,0,2) != substr($lang_from_http_accept[0],0,2)) {
            //echo "Non trouvé, 2ème tentative";
            $lang_from_http_accept = explode('-', $lang_from_http_accept[0]);
            //echo $lang_from_http_accept[0].'<br />';
            $locale = lang2locale($lang_from_http_accept[0]);
        }
        //echo $locale.'<br />';
        $localeshort=locale2lang($locale);
    }
}

// Définition de la langue :
$results=putenv("LC_ALL=$locale.utf8");
if (!$results) {
    exit ('putenv failed');
}
$results=putenv("LC_LANG=$locale.utf8");
if (!$results) {
    exit ('putenv failed');
}
$results=putenv("LC_LANGUAGE=$locale.utf8");
if (!$results) {
    exit ('putenv failed');
}
$results=setlocale(LC_ALL, "$locale.utf8");
if (!$results) {
    exit ('setlocale failed: locale function is not available on this platform, or the given local does not exist in this environment');
}
bindtextdomain("messages", "./lang");
textdomain("messages");
/* / language */

if (convertHumain2octect($config['maxUploadPerFile']) > convertHumain2octect(ini_get('upload_max_filesize')))  {
	exit(printf(_('In coherence with your configuration (config.yaml) you must increase the PHP configuration upload_max_filesize to %s'), $config['maxUploadPerFile']));
}
if (convertHumain2octect($config['maxUploadPerFile']) > convertHumain2octect(ini_get('post_max_size')))  {
	exit(printf(_('In coherence with your configuration (config.yaml) you must increase the PHP configuration post_max_size to %s'), $config['maxUploadPerFile']));
}
if (convertHumain2octect($config['maxUploadNb']) > convertHumain2octect(ini_get('max_file_uploads')))  {
	exit(printf(_('In coherence with your configuration (config.yaml) you must increase the PHP configuration max_file_uploads to %s'), $config['maxUploadNb']));
}

// Password
$passwordEnter=false;
if (isset($_POST['password'])) {
    //session_start();
    setcookie("password".$id,base64_encode($config['passwordUniqKey'].$_POST['password']),strtotime( $config['passwordTimeRemember'] ), '/');
    $passwordEnter=base64_encode($config['passwordUniqKey'].$_POST['password']);
} elseif (isset($_COOKIE['password'.$id])) {
    $passwordEnter=$_COOKIE['password'.$id];
}
$uploadDirId=$config['uploadDir'].'/'.$id.'/';
$passwordInvalid=false;
$passwordForm=false;
if (is_file($uploadDirId.'/.password.cfg') && $passwordEnter != false) {
    $countPasswordChar = strlen(base64_decode($passwordEnter))-strlen($config['passwordUniqKey']);
    $password = substr(base64_decode($passwordEnter), -$countPasswordChar);
    if (password_verify($config['passwordUniqKey'].$password, file_get_contents($uploadDirId.'/.password.cfg'))) {
        $passwordForm=false;
    } else {
        $passwordForm=true;
        $passwordInvalid=true;
    }    
} elseif (is_file($uploadDirId.'/.password.cfg')) {
    $passwordForm=true;
}
if ($passwordForm == false) {
    // ZIP, READ ou DL
    if (isset($_GET['action']) && ($_GET['action'] == 'zip' || $_GET['action'] == 'dl' || $_GET['action'] == 'read')) {
        if ($_GET['action'] == 'zip') {
            genZip($id);     
            $filename = $id.'.zip'; 
            $contentType='application/zip';
        } elseif ($_GET['action'] == 'dl' || $_GET['action'] == 'read') {
            $filename = $_GET['file'];
            $contentType=mime_content_type($uploadDirId.$filename);
        }
        // HTTP Headers File Downloads
        // https://perishablepress.com/press/2010/11/17/http-headers-file-downloads/
        
        // http headers for zip downloads
        if (headers_sent()) {
            echo 'HTTP header already sent';
        } else {
            if (!is_file($uploadDirId.$filename)) {
                header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found');
                header('Location: '.$config['baseUrl'].'404');
            } else if (!is_readable($uploadDirId.$filename)) {
                header($_SERVER['SERVER_PROTOCOL'].' 403 Forbidden');
                header('Location: '.$config['baseUrl'].'403');
            } else {
                header($_SERVER['SERVER_PROTOCOL'].' 200 OK');
                header("Pragma: public");
                if ($_GET['action'] != 'read') {
                    header("Expires: 0");
                    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                    header("Cache-Control: public");
                    header("Content-Description: File Transfer");
                    header("Content-Disposition: attachment; filename=\"".$filename."\"");
                    header("Content-Transfer-Encoding: binary");
                    header("Content-Length: ".filesize($uploadDirId.$filename));
                }
                header("Content-Type: ".$contentType);
                @readfile($uploadDirId.$filename);
                // Access count
                if (!preg_match('/^\.(.+)\.small$/', $filename) && is_file($uploadDirId.'/.access.cfg')) {
                    $nbAccess = file_get_contents($uploadDirId.'/.access.cfg')-1;
                    file_put_contents($uploadDirId.'/.access.cfg', $nbAccess);
                    if (! $nbAccess > 0) {
                        rrmdir($uploadDirId);
                    }
                }
                
            }
        }
        exit();
    }

    // Del
    if (isset($_GET['action']) && $_GET['action'] == 'del') {
        //echo $config['uploadDir'].'/'.$id.'/.key-'.$_GET['key'];
        //print_r($_GET);
        if (! is_file($config['uploadDir'].'/'.$id.'/.key-'.$_GET['key'].'.cfg')) 
            exit('No hack 5 - delete not Unauthorized');
        // Si c'est juste un fichier
        if (isset($_GET['file'])) {
            @unlink($config['uploadDir'].'/'.$id.'/'.$_GET['file']);
            @unlink($config['uploadDir'].'/'.$id.'/.'.$_GET['file'].'.small');
            // Checksum
            $checksum = new Checksum();
            $deleteFile = $checksum->deleteFile($config['uploadDir'].'/'.$id.'/'.$_GET['file']);
            //error_log('checksum>deleteFile : '.$config['uploadDir'].'/'.$id.'/'.$_GET['file']);
            //error_log('checksum>deleteFile return    : '.json_encode($deleteFile));
            if ($deleteFile != true) {
                exit("deleteFile : ".json_encode($deleteFile));
            }
            // Si c'est le dernire, on supprime le tout
            $uploadDirId = $config['uploadDir'].'/'.$id.'/';
            $nbFile=0;
            foreach (scandir($uploadDirId) as $file) {
                if (is_file($uploadDirId.'/'.$file) 
                && $file != $id.'.zip'
                && !preg_match('/^\.(.+)\.cfg$/', $file)
                && !preg_match('/^\.(.+)\.small$/', $file)) {
                    $nbFile++;
                }
            }
            if ($nbFile == 0) {
                rrmdir($config['uploadDir'].'/'.$id);
            }
        } else {
        // Si c'est le dossier
            rrmdir($config['uploadDir'].'/'.$id);
            $nbFile = 0;
        }
        @include_once('./header.php'); 
        if ($nbFile == 0) {
            header('Status: 301 Moved Permanently', false, 301);
            header('Location: '.$config['baseUrl']);
        } else {
            header('Status: 301 Moved Permanently', false, 301);      
            header('Location: '.$config['baseUrl'].'/'.$id.'/');
        }
        exit();
    }
}
@include_once('./header.php'); 
?> 
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <title><?= $config['shortTitle'] ?> : <?= $config['title'] ?></title>
        <script src="<?= $config['baseUrl'] ?>lib/jquery-3.1.0.min.js"></script>
        <script>
			var Config_baseUrl = '<?= $config['baseUrl'] ?>'
            var Config_maxUploadPerFile = <?= convertHumain2octect($config['maxUploadPerFile']) ?> 
            var Config_maxUploadTotal = <?= convertHumain2octect($config['maxUploadTotal']) ?> 
            var Config_maxUploadNb = <?= $config['maxUploadNb'] ?> 
            var Msg_errorFileSize = '<?php  printf(_('this file exceeds the allowed size %s'), $config['maxUploadPerFile']) ?>';
            var Msg_errorTotalSize = '<?php  printf(_('The total size of the files exceeds the allowed size : %s'), $config['maxUploadTotal']) ?>';
            var Msg_errorUploadNb = '<?php  printf(_('You can not send more than %d files at a time'), $config['maxUploadNb']) ?>';
            var Msg_errorFileType = '<?php  printf(_('this type of file isn\\\'t allow')) ?>';
            var filesUploadQueu=0;
        </script>

        <script src="<?= $config['baseUrl'] ?>lib/upload.js"></script>

        <link rel="stylesheet" href="<?= $config['baseUrl'] ?>lib/style.css" />
        <link rel="stylesheet" href="<?= $config['baseUrl'] ?>lib/css-file-icons.css" />
        <link rel="apple-touch-icon" sizes="180x180" href="<?= $config['baseUrl'] ?>/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?= $config['baseUrl'] ?>/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?= $config['baseUrl'] ?>/favicon-16x16.png">
        <link rel="manifest" href="<?= $config['baseUrl'] ?>/site.webmanifest">
        <link rel="stylesheet" href="<?= $config['baseUrl'] ?>lib/jquery-ui.min.css">
        <link rel="stylesheet" href="<?= $config['baseUrl'] ?>lib/lightgallery.min.css">
        <script src="<?= $config['baseUrl'] ?>lib/jquery-ui.min.js"></script>
        <script src="<?= $config['baseUrl'] ?>lib/lightgallery.min.js"></script>
        <script>
        $( function() {
            $( document ).tooltip();
        } );
        </script>
        <!-- Bootstrap styles -->
<!--
        <link rel="stylesheet" href="<?= $config['baseUrl'] ?>lib/bootstrap.min.css" />
-->
        <link
          rel="stylesheet"
          href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
          crossorigin="anonymous"
        />
        <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
        <link rel="stylesheet" href="<?= $config['baseUrl'] ?>lib/jQuery-File-Upload/css/jquery.fileupload.css" />
        <link rel="stylesheet" href="<?= $config['baseUrl'] ?>lib/jQuery-File-Upload/css/jquery.fileupload-ui.css" />
    </head>
    <body>
        <div id="langues">
            <?php 
			foreach($langueEtLocalDispo as $langShort=>$lang) {
				$flag='';
                
				if ($localeshort == $langShort) {
					$flag=' drapeauActif';
				}
				echo '<a id="href'.$langShort.'" href="?langueChange='.$langShort.'"><img class="drapeau'.$flag.'" src="'.$config['baseUrl'].'lib/'.$langShort.'.png" alt="'.$langShort.'" width="23" height="15" /></a>';
			}
			?>
        </div>
        <div id="languesLegende" style="display: none"></div>
        <div id="page-wrap">         
        <?php        
            if ($config['maintenanceMod'] == true && $config['mainteneurIp'] != $_SERVER['REMOTE_ADDR'])  {
                echo '<h1>'._('Maintenance').'</h2>';
                echo '<p>'.$config['maintenanceMsg'].'</p>';
            } else {
            
            // signaler au mainteneur que le mode maintenance est à true
            if ($config['maintenanceMod'] == true)  {
                echo '<p>'._('Mode Maintenance as true').'</p>';
            } 
            
            @include_once('./header-page.php'); 
            $similarServicesLink='';
            if ($config['similarServicesView']) {
                //~ $similarServicesLink='<div class="similarServices">
                //~ <div class="similarHref"><a href="#similarServices" id="similarServices">'._('Similar services').'</a></div>
                //~ <div class="similarLink">';
                //~ $similarServicesLink.= '<script type="text/javascript" src="'.$config['baseUrl'].'/lib/alternative-chatons.js"></script>';
                //~ $similarServicesLink.= '<p id="entraide-chatons-alternative"></p>';
                //~ $similarServicesLink.='</div></div>';
                $similarServicesLink='<div class="similarServices">
                <div class="similarHref">'._('Similar services').' :</div>
                <div>';
                $similarServicesLink.= '<script type="text/javascript" src="'.$config['baseUrl'].'/lib/alternative-chatons.js"></script>';
                $similarServicesLink.= '<p id="entraide-chatons-alternative"></p>';
                $similarServicesLink.='</div></div>';
            } 
            $uploadDirId=$config['uploadDir'].'/'.$id;
            $action = null;
            if (isset($_GET['action'])) {
                $action = $_GET['action'];
            }
            if (!is_writable($config['uploadDir'])) {
                $action = 'ErrorUploadDir';
            }
            if ($action == 'html' && !is_dir($config['uploadDir'].'/'.$id)) {
                $action = '404';
            }
            if (!$config['htmlPages'] && $action == 'page')  {
                $action = '404';
            }
            if ($passwordForm == true) {
                $action = 'password';
            }
            $echoNewUploadPub = '<div id="newUploadPub"><a href="'.$config['baseUrl'].'">'._('Send / share your files').'</a></div>';
            $echoNewUpload = '<div id="newUpload" class="newUpload"><noscript><h3>You must have JavaScript enabled in order to use this site. Please enable JavaScript and then reload this page in order to continue. </h3> </noscript>
            <!-- The file upload form used as target for the file upload widget -->
            <form
            id="fileupload"
            action=""
            method="POST"
            enctype="multipart/form-data"
            >
                <input type="hidden" value="" id="files_key" name="key" />
                <input type="hidden" value="" id="files_id" name="id" />
                <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
                <div class="fileupload-buttonbar">
                <div class="expire-button">'._('Expire').' : <select name="expire" id="expire">';
                    foreach ($config['expireDay'] as $expireDay) {
                        $dayOrDays=_('days');
                        if ($expireDay == 1) {
                            $dayOrDays=_('day');
                        }
                        if ($expireDay == $config['expireDayDefault']) {
                            $echoNewUpload .= '<option value="'.$expireDay.'"  selected="selected">'.$expireDay.'  '.$dayOrDays.'</option>';
                        } else {
                            $echoNewUpload .= '<option value="'.$expireDay.'" >'.$expireDay.'  '.$dayOrDays.'</option>';
                        }
                    } 
                $echoNewUpload .= '
                </select></div>
                  <div class="boutton">
                    <!-- The fileinput-button span is used to style the file input field as button -->
                    <span id="ButtonAdd" class="btn btn-success fileinput-button">
                      <i class="glyphicon glyphicon-plus"></i>
                      <span>'._('Add files...').'</span>
                      <input type="file" name="files[]" multiple />
                    </span>
                    <button id="ButtonStart" type="submit" class="btn btn-primary start">
                      <i class="glyphicon glyphicon-upload"></i>
                      <span>'._('Start upload').'</span>
                    </button>
                    <button id="ButtonReset" type="reset" class="btn btn-warning cancel">
                      <i class="glyphicon glyphicon-ban-circle"></i>
                      <span>'._('Cancel upload').'</span>
                    </button>
                    <button id="ButtonMyFiles" type="reset" onclick="location.href=\''.$config['baseUrl'].'/My\';" class="bulles myFiles btn btn-info">
                      <i class="glyphicon glyphicon-folder-open"></i>
                      <span>'._('See the files already sent').'</span>
                    </button>
                    <!-- The global file processing state -->
                    <span class="fileupload-process"></span>
                  </div>
                  <div id="redirectToFiles">Redirect to '.$config['baseUrl'].'<span id="redirectToFilesId"></span>/ in progress...</div>
                  <div  class="error" id="maxUploadTotalError">'.
                  _('The total limit per upload is ').$config['maxUploadTotal']
                  .'</div>
                  <!-- The global progress state -->
                  <div class="fileupload-progress fade">
                    <!-- The global progress bar -->
                    <div
                      class="progress progress-striped active"
                      role="progressbar"
                      aria-valuemin="0"
                      aria-valuemax="100"
                    >
                      <div
                        class="progress-bar progress-bar-success"
                        style="width: 0%;"
                      ></div>
                    </div>
                    <!-- The extended global progress state -->
                    <div class="progress-extended">&nbsp;</div>
                  </div>
                </div>
                <!-- The table listing the files available for upload/download -->
                <table role="presentation" class="table table-striped">
                  <tbody class="files"></tbody>
                </table>
              
              <div class="shareUrlPrint file fileJust1 read input">'._('The sharing will be accessible from').' : <input class="copy read fileAll" name="read" type="text" value="'.$config['baseUrl'].$id.'/" id="shareUrl" readonly=""></div>
              
            <a id="uploadOptionsLinkShow" class="uploadOptionsLink">'._('Options').' &#8642;</a>
            <div id="uploadOptions">
                <a id="uploadOptionsLinkHide"  class="uploadOptionsLink">'._('Options').' &#8638;</a>
                <p id="resizeForm">'._('Images resize').' : <select name="resize" id="resize">';
                foreach ($config['imageResize'] as $imageResize) {
                    $imageResizeName = $imageResize.'px';
                    $imageResizeValue = $imageResize;
                    if ($imageResize == 0) {
                        $imageResizeName = _('No resizing');
                        $imageResizeValue=999999;
                    }
                    if ($imageResize == $config['imageResizeDefault']) {
                        $echoNewUpload .= '<option value="'.$imageResize.'"  selected="selected">'.$imageResizeName.'</option>';
                    } else {
                        $echoNewUpload .= '<option value="'.$imageResize.'" >'.$imageResizeName.'</option>';
                    }
                } 
                $echoNewUpload .= '</select></p>
                <p id="uploadOptionPassword"><input type="checkbox" name="passwordCheckbox" id="passwordCheckbox" />'._('Protect with password').'<span id="passwordForm"> : <br /><input type="password" name="password" id="password" autocomplete="off" /></span></p>
                <p id="uploadOptionAccess"><input type="checkbox" name="accessCheckbox" id="accessCheckbox" />'._('Delete after access').'<span id="accessForm"> : <br /><input title="'._('Number of accesses before deletion (1 minimum)').'" type="number" min="1" max="999999" step="1"  name="access" id="access" value="'.$config['deleteAfterAccessDefault'].'" /></span></p>
            </div>
            </form>
            <div>
            </div>
            <div class="limit"><p>'.
            _('The limit per file is ').$config['maxUploadPerFile']
            .'</p><p>'.
            _('The total limit per upload is ').$config['maxUploadTotal']
            .'</p></div>
             <script>
                if (localStorage.getItem(\'myFiles\')) {
                    $(\'.myFiles\').show();
                }
            </script></div>';

            
            if ($config['authImap'] ==  true || $config['authGuest'] ==  true) {
                $authentification=false;
                // Vérification de l'authentification / du token
                if (isset($_COOKIE['token'])) {
                    $checkToken = $auth->tokenExist($_COOKIE['token']);
                    if ($checkToken == true) {
                        $authentification=true;
                    }
                    // Afficher l'erreur si elle est présente
                    if (isset($checkToken['error'])) {
                        echo $checkToken['error'];
                    }
                }
                
                if ($authentification == false) {
                    $echoNewUpload='
                    <div class="container-fluid">';
                    if (isset($config['authHelp'])) {
                        $echoNewUpload.='<p>'.$config['authHelp'].'</p>';
                    }
                    $echoNewUpload.='<div class="row">
                    <form method="post" action="#" class="form-horizontal">';
                        if (isset($_POST['loginForm']) && empty($authReturn['token'])) {
                            $echoNewUpload.='<p class="highlight-1">'._('Authentification failed').'</p>';
                        }
                        if ($config['authImap'] ==  true) {
                            $echoNewUpload.='<div id="authImap" class="col-xs-6">
                                <h2>'._('E-mail authentification').'</h2>';
                                if (isset($config['authImapHelp'])) {
                                    $echoNewUpload.='<p>'.$config['authImapHelp'].'</p>';
                                }
                                $echoNewUpload.='
                                <div class="form-group">
                                    <label for="imapLogin" class="control-label">'._('Your e-mail').'</label>
                                    <input type="email" name="imapLogin" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label for="imapPassword" class="control-label">'._('Your password').'</label>
                                    <input type="password" name="imapPassword" class="form-control" />   
                                </div>
                            </div>';
                        }
                        if ($config['authGuest'] ==  true) {
                            $echoNewUpload.='<div id="authGuest" class="col-xs-6">
                                <h2>'._('Guest authentification').'</h2>';
                                if (isset($config['authGuestHelp'])) {
                                    $echoNewUpload.='<p>'.$config['authGuestHelp'].'</p>';
                                }
                                $echoNewUpload.='
                                <label for="guestPassword" class="control-label">'._('The guest password').'</label>
                                <input type="password" name="guestPassword" class="form-control" />
                            </div>';
                        }
                        $echoNewUpload.='<div id="authSubmit" class="col-xs-12">
                        <div class="col-xs-6">
                            <label for="dateExpire" class="col-xs-6">'._('Remember me during').'</label>
                            <div class="col-xs-6">
                                <select  name="dateExpire" class="form-control">';
                                foreach ($config['authExpire'] as $key => $value) {
                                    $echoNewUpload .= '<option value="'.$key.'">'.$value.'</option>';
                                }
                                $echoNewUpload.='
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <input class="btn btn-primary form-control" type="submit" name="loginForm" value="'._('Connect').'" />
                        </div>
                        </div>
                    </form>
                    </div>
                    </div>';
                } else {
                    $echoNewUpload.='<div class="newUpload"><button id="logout" class="btn btn-warning cancel">
                        <i class="glyphicon glyphicon-log-out"></i>
                        <span>'._('Logout').'</span>
                    </button></div>';
                }
            }
            switch ($action) {
                case 'ErrorUploadDir':
                    echo '<div class="highlight-1">';
                    printf(_('Error: The directory (%s) is not writable, please contact the service administrator'), $config['uploadDir']);
                    echo '</div>';
                    break;
                case 'password':
                    echo '<h1><a href="'.$config['baseUrl'].'">'.$config['shortTitle'].'</a> : '.$config['title'].'</h1>';
                    echo $echoNewUploadPub;
                    if (isset($_POST['password']) && $passwordInvalid == true) {
                        echo '<div class="highlight-1">';
                        echo _('Error: Incorrect password');
                        echo '</div>';
                    }
                    echo '<div class="passwordForm">';
                    echo '<p>'._('This file is protected by a password, thank you to indicate it below').'</p>';
                    echo '<div><form action="#" method="post">';
                    echo '<input type="password" name="password" />';
                    echo '<input type="submit" />';
                    echo '</form></div>';
                    echo '</div>';
                    echo $echoNewUpload;
                    break;
                case 'page':
                    foreach ($config['htmlPages'] as $fileName => $name) {
                        if ($_GET['name'] == $fileName) {
                            $page['fileName'] = $fileName;
                            $page['name'] = $name;
                        }
                    }
                    if (empty($page['name'])) {
                        echo '<p>'._('The requested page does not exist').'</p>';
                    } elseif (!is_file('htmlPages/'.$page['fileName'].'.html')) {
                        echo '<p>'._('The requested page does not exist').'</p>';
                    } else {
                        include('htmlPages/'.$page['fileName'].'.html');
                    }
                    echo $echoNewUpload;
                    break;
                case 'html': 
                    echo '<h1><a href="'.$config['baseUrl'].'">'.$config['shortTitle'].'</a> : '.$config['title'].'</h1>';
                    echo $echoNewUploadPub;
                    $expire=explode('-', $id);
                    $dateExpire=date('d/m/Y H:m', $expire[0]);
                    $dStart = new DateTime(date('Y-m-d', $expire[0]));
                    $dEnd  = new DateTime(date('Y-m-d'));
                    $dDiff = $dStart->diff($dEnd);
                    $dayExpire = $dDiff->format('%a');
                    $classExpire ='';
                    if ($dayExpire < 2) {
                        $classExpire='verySpeedDownload';
                    }elseif ($dayExpire < 5) {
                        $classExpire='speedDownload';
                    }
                    printf('<p class="'.$classExpire.'">'._('These files will be automatically deleted on %s, ie in %d days').'</p>', $dateExpire, $dayExpire);
                    if (is_file($uploadDirId.'/.access.cfg') ) {
                        $nbAccess = file_get_contents($uploadDirId.'/.access.cfg');
                        if ($nbAccess < 3) {
                            $classExpireAccess='verySpeedDownload';
                        }elseif ($nbAccess < 6) {
                            $classExpireAccess='speedDownload';
                        }
                        printf('<p class="'.$classExpireAccess.'">'._('These files will be automatically deleted in %d access').'</p>', $nbAccess);
                    }
                    $nbFile=0;
                    $nbImageFile=0;
                    foreach (scandir($uploadDirId) as $file) {
                        if (is_file($uploadDirId.'/'.$file) 
                        && $file != $id.'.zip'
                        && !preg_match('/^\.(.+)\.cfg$/', $file)
                        && !preg_match('/^\.(.+)\.small$/', $file)) {
                            $filesInUploadDirId[]=$file;
                            $nbFile++;
                            if (preg_match('/^image\/(png|jpeg|gif)$/', mime_content_type($uploadDirId.'/'.$file))) {
                                $nbImageFile++;
                            }
                        }
                    }
                    if ($nbImageFile == $nbFile) {
                        echo '<div id="viewMod">';
                        if (isset($_GET['gallery'])) {
                            echo '<a href="'.$config['baseUrl'].$id.'/">'._('View in list mode ').'</a>';
                        } else {
                            echo '<a href="'.$config['baseUrl'].'gallery/'.$id.'/">'._('View in gallery mode ').'</a>';
                        }
                        echo '</div>';
                    }
                    if ($nbFile == 0) {
                        echo '<p>'._('Error: Nothing to display').'</p>';
                    } elseif ($nbFile == 1) {
                        $linkDownload=$config['baseUrl'].$id.'/';
                        echo '<div class="viewNormal fileGlobal fileJust1 file-ext-html" >';
                            echo '<div class="file fileJust1 icone" onclick="location.href=\''.$linkDownload.'\'"><div class="fi fi-html fi-size"><div class="fi-content">html</div></div></div>';
                            echo '<div class="file fileJust1 name"><a href="'.$linkDownload.'" target="_blank">'._('This page').'</a></div>';
                            echo '<div class="file fileJust1 read input"><a href="'.$config['baseUrl'].$id.'/" target="_blank"><img src="'.$config['baseUrl'].'/lib/eye.svg" /></a> <input class="copy read fileAll" name="read" type="text" value="'.$config['baseUrl'].$id.'/" readonly=""></div>';
                        echo '</div>';
                    }
                    
                    if (isset($_GET['gallery'])) {
                        echo '<div id="lightgalleryGallery">';
                            $idFile=0;
                            foreach ($filesInUploadDirId as $fileInUploadDirId) {
                                $pathInfo = pathinfo($uploadDirId.'/'.$fileInUploadDirId);
                                $linkDownload=$config['baseUrl'].'dl/'.$id.'/'.$fileInUploadDirId;
                                echo '<div class="viewGallery file'.$idFile.' file-ext-'.$pathInfo['extension'].'" >';
                                    echo '<div style="float: left; border: 1px solid #C6C6C6;" class="file file'.$idFile.' icone">';
                                    if (preg_match('/^image\/(png|jpeg|gif)$/', mime_content_type($pathInfo['dirname'].'/'.$pathInfo['basename']))) {
                                        if (!is_file($pathInfo['dirname'].'/.'.$pathInfo['basename'].'.gallery.small')) {
                                            resize_image($pathInfo['dirname'].'/'.$pathInfo['basename'], $pathInfo['dirname'].'/.'.$pathInfo['basename'].'.gallery.small' , 330, 330);
                                        }
                                        if (is_file($pathInfo['dirname'].'/.'.$pathInfo['basename'].'.gallery.small')) {
                                            echo '<div class="item" data-src="'.$config['baseUrl'].$id.'/'.$fileInUploadDirId.'"><img width="330" src="'.$config['baseUrl'].$id.'/.'.$pathInfo['basename'].'.gallery.small" /></a></div>';
                                        } else {
                                            echo '<a href="'.$linkDownload.'" target="_blank"><div class="fi fi-'.$pathInfo['extension'].' fi-size-xl"><div class="fi-content">'.$pathInfo['extension'].'</div></div></a>';
                                        }
                                    } else {
                                        echo '<a href="'.$linkDownload.'" target="_blank"><div class="fi fi-'.$pathInfo['extension'].' fi-size-xl"><div class="fi-content">'.$pathInfo['extension'].'</div></div></a>';
                                    }
                                    echo '</div>';
                                echo '</div>';
                            }
                        echo '</div>';
                    }
                    
                    if ($nbFile > 1) {
                        $linkDownload=$config['baseUrl'].$id.'.zip';                        
                        echo '<div style="clear : both" class="fileGlobal fileAll file-ext-zip" >';
                            echo '<div class="file fileAll icone" onclick="location.href=\''.$linkDownload.'\'"><div class="fi fi-zip fi-size-lg"><div class="fi-content">zip</div></div></div>';
                            echo '<div class="file fileAll name"><a href="'.$linkDownload.'" target="_blank">'._('All').'</a><a class="delete deleteLinkAll bulles"  rel="tooltip" title="'._('Delete all (permanently)').'" href="'.$config['baseUrl'].'del/'.$id.'/KEYHERE/">  -  <img width="15" src="'.$config['baseUrl'].'/lib/trash.svg" /></a></div>';
                            echo '<div class="file fileAll read input"><a href="'.$config['baseUrl'].$id.'/"><img src="'.$config['baseUrl'].'/lib/eye.svg" /></a> <a rel="tooltip" class="bulles" title="'._('Click to copy the link to the clipboard').'"><input class="copy read fileAll" name="read" type="text" value="'.$config['baseUrl'].$id.'/" readonly=""></a></div>';
                            if ($nbImageFile == $nbFile) {
                                echo '<div class="file fileAll read input"><a href="'.$config['baseUrl'].'gallery/'.$id.'/"><img src="'.$config['baseUrl'].'/lib/gallery.svg" /></a> <a rel="tooltip" class="bulles" title="'._('Click to copy the link to the clipboard').'"><input class="copy read fileAll" name="read" type="text" value="'.$config['baseUrl'].'gallery/'.$id.'/" readonly=""></a></div>';
                            }
                            echo '<div class="file fileAll dl input"><a href="'.$linkDownload.'"><img src="'.$config['baseUrl'].'/lib/download.svg" /></a> <a rel="tooltip" class="bulles" title="'._('Click to copy the link to the clipboard').'"><input class="copy dl fileAll" name="dl" type="text" value="'.$linkDownload.'" readonly=""></a></div>';
                        echo '</div>';
                    }
                    if (!isset($_GET['gallery'])) {
                        $idFile=0;
                        echo '<div id="lightgalleryNormal">';
                        foreach ($filesInUploadDirId as $fileInUploadDirId) {
                            $pathInfo = pathinfo($uploadDirId.'/'.$fileInUploadDirId);
                            $linkDownload=$config['baseUrl'].'dl/'.$id.'/'.$fileInUploadDirId;
                            echo '<div class="viewNormal fileGlobal file'.$idFile.' file-ext-'.$pathInfo['extension'].'" >';
                                echo '<div class="file file'.$idFile.' icone delete"><a href="'.$config['baseUrl'].'del/'.$id.'/KEYHERE/'.$pathInfo['basename'].'"  class="deleteLink">azer<img src="'.$config['baseUrl'].'/lib/trash.svg" /></a></div>';
                                echo '<div class="file file'.$idFile.' icone">';
                                if (preg_match('/^image\/(png|jpeg|gif)$/', mime_content_type($pathInfo['dirname'].'/'.$pathInfo['basename']))) {
                                    if (!is_file($pathInfo['dirname'].'/.'.$pathInfo['basename'].'.small')) {
                                        resize_image($pathInfo['dirname'].'/'.$pathInfo['basename'], $pathInfo['dirname'].'/.'.$pathInfo['basename'].'.small' , 60, 60);
                                    }
                                    if (is_file($pathInfo['dirname'].'/.'.$pathInfo['basename'].'.small')) {
                                        echo '<div class="item" data-src="'.$config['baseUrl'].$id.'/'.$fileInUploadDirId.'"><img width="55" src="'.$config['baseUrl'].$id.'/.'.$pathInfo['basename'].'.small" /></a></div>';
                                    } else {
                                        echo '<a href="'.$linkDownload.'" target="_blank"><div class="fi fi-'.$pathInfo['extension'].' fi-size-lg"><div class="fi-content">'.$pathInfo['extension'].'</div></div></a>';
                                    }
                                } else {
                                    echo '<a href="'.$linkDownload.'" target="_blank"><div class="fi fi-'.$pathInfo['extension'].' fi-size-lg"><div class="fi-content">'.$pathInfo['extension'].'</div></div></a>';
                                }
                                echo '</div>';
                                echo '<div class="file file'.$idFile.' name"><a href="'.$linkDownload.'" target="_blank">'.truncate($fileInUploadDirId, 40, '...', true).'</a></div>';
                                echo '<div class="file file'.$idFile.' info">';
                                    echo '<span class="size file'.$idFile.'">'._('Size').' : '.convertOctect2humain(filesize($pathInfo['dirname'].'/'.$pathInfo['basename'])).'</span> ';
                                    echo '<span class="type file'.$idFile.'">'._('Type').' : '.mime_content_type($pathInfo['dirname'].'/'.$pathInfo['basename']).'</span> ';
                                    //echo '<span class="size">'._('Type').' : '.mime_content_type($pathInfo['dirname'].'/'.$pathInfo['basename']).'</span> ';
                                echo '</div>';
                                echo '<div class="file file'.$idFile.' read input"><a href="'.$config['baseUrl'].$id.'/'.$fileInUploadDirId.'" target="_blank"><img src="'.$config['baseUrl'].'/lib/eye.svg" /></a> <a rel="tooltip" class="bulles" title="'._('Click to copy the link to the clipboard').'"><input class="copy read file'.$idFile.'" name="read" type="text" value="'.$config['baseUrl'].$id.'/'.$fileInUploadDirId.'"></a></div>';
                                echo '<div class="file file'.$idFile.' dl input"><a href="'.$linkDownload.'" target="_blank"><img src="'.$config['baseUrl'].'/lib/download.svg" /></a> <a rel="tooltip" class="bulles" title="'._('Click to copy the link to the clipboard').'"><input class="copy dl file'.$idFile.'" name="dl" type="text" value="'.$linkDownload.'" readonly=""></a></div>';
                            echo '</div>';
                            $idFile++;
                        }
                        echo '</div>';
                    }
                    echo '<div id="addToShare"><h3>'._('Add files to this share').'</h3>';
                    echo $echoNewUpload;
                    echo '</div>';
                    ?>
                    <script type="text/javascript">
                        $(document).ready(function() {
                            // lightgallery
                            // https://github.com/sachinchoolur/lightGallery
                            $("#lightgalleryNormal").lightGallery({
                                selector: '.item'
                            });
                            $("#lightgalleryGallery").lightGallery({
                                selector: '.item'
                            });
                        });
                        // Affiche les bouton de suppression si c'est le poste qui a uploadé
                        if (localStorage.getItem('myFiles')) {
                            var storageMyFiles = JSON.parse(localStorage.getItem('myFiles'));
                            if (storageMyFiles.items.filter(function(e) { return e.id === '<?= $id ?>'; }).length > 0) {
                                var searchWithId = storageMyFiles.items.filter(function(e) { return e.id === '<?= $id ?>'; })
                                var keyForThis = Object.values(searchWithId[0])[1];
                                $('.deleteAll').show();
                                $('.delete').show();         
                                $('#files_key').val(keyForThis);    
                                $('#uploadOptionPassword').hide();    
                                $('#uploadOptionAccess').hide();    
                                $('.expire-button').hide();
                                $('#addToShare').show();         
                                var links = $('.deleteLink');
                                for(var i = 0; i< links.length; i++){
                                    var oldLink = links[i].href;
                                    var newLink = oldLink.replace('KEYHERE', keyForThis);
                                    links[i].href = newLink;
                                }
                                var links = $('.deleteLinkAll');
                                for(var i = 0; i< links.length; i++){
                                    var oldLink = links[i].href;
                                    var newLink = oldLink.replace('KEYHERE', keyForThis);
                                    links[i].href = newLink;
                                }
                            } else {
                                $('#addToShare').hide();
                            }
                        } else {
                            $('#addToShare').hide();
                        }
                        
                        function deleteLast(href) {
                            var hrefSplit = href.split('\/');
                            var id = false;
                            var key = false;
                            for (var i = 0; i < hrefSplit.length; i++) {
                                var regexTimestamp = RegExp('^[0-9]+-[0-9]{1,2}$');
                                if (regexTimestamp.test(hrefSplit[i])) {
                                    id=hrefSplit[i];
                                }
                                if (parseInt(hrefSplit[i]) >= 100000000000 && parseInt(hrefSplit[i]) <= 999999999999) {
                                    key=hrefSplit[i];
                                }
                            }
                            if (id != false && key != false && localStorage.getItem('myFiles')) {
                                var storageMyFiles = JSON.parse(localStorage.getItem('myFiles'));
                                var newData = {items: []};
                                for (var i = 0; i < storageMyFiles.items.length; i++) {
                                    var idSplit=storageMyFiles.items[i].id.split("-");
                                    var timestampNow = Number(new Date());
                                    var timestampExpire = idSplit[0]*1000;
                                    // Ne pas afficher les expiré
                                    // Et si ce n'est pas celui qu'on efface
                                    if (timestampNow < timestampExpire
                                    && storageMyFiles.items[i].id != id
                                    && storageMyFiles.items[i].key != key) {
                                        // On update le maintient dans le stroage
                                        newData.items.push(
                                                {id: storageMyFiles.items[i].id, key: storageMyFiles.items[i].key}
                                            );
                                    } 
                                }
                                // Ecriture du storage
                                localStorage.setItem('myFiles', JSON.stringify(newData));
                            }
                        }
                        
                        $(".deleteLinkAll").on('click',function(){
                            if (confirm("<?= _('Are you sure you want to delete everything?') ?>")) {
                                deleteLast(this.href);
                                return true;
                            } else {
                                return false;
                            }
                        });    
                        $(".deleteLink").on('click',function(){
                            if (confirm("<?= _('Are you sure you want to delete it?') ?>")) {
                                <?php 
                                // Si c'est le dernier, on supprime l'enregistrement localStorage
                                if ($nbFile == 1) { 
                                    echo "deleteLast(this.href);";
                                }
                                ?>
                                return true;
                            } else {
                                return false;
                            }
                        });    
                        
                    </script>
                    <?php
                    break;
                case 'myFiles':
                    ?>
                    <h1><?= $config['shortTitle'] ?> : <?= _('My files') ?></h1>
                    <?= $echoNewUploadPub ?>
                    <p><?= _('Online file sharing service <a href="https://en.wikipedia.org/wiki/Open_source">free of rights</a> (license <a href="https://en.wikipedia.org/wiki/Beerware">Beerware</a>) and free.') ?></p>
                    <table id="myFilesTab">
                            <tr><th> - </th><th><?= _('Nb of files') ?></th><th><?= _('Creation date ') ?></th><th><?= _('Expiration date') ?></th><th><?= _('Size') ?></th><th><?= _('Remaining access') ?></th><th><?= _('Password') ?></th><th><?= _('Link') ?></th></tr>
                    </table>
                    <script>
                        function sleep(milliseconds) {
                          const date = Date.now();
                          let currentDate = null;
                          do {
                            currentDate = Date.now();
                          } while (currentDate - date < milliseconds);
                        }
                        var fileNotExpire=0;
                        if (localStorage.getItem('myFiles')) {
                            var storageMyFiles = JSON.parse(localStorage.getItem('myFiles'));
                            var newData = {items: []};
                            for (var i = 0; i < storageMyFiles.items.length; i++) {
                                var idSplit=storageMyFiles.items[i].id.split("-");
                                var timestampNow = Number(new Date());
                                var timestampExpire = idSplit[0]*1000;
                                // Ne pas afficher les expiré
                                if (timestampNow < timestampExpire) {
                                    var dateExpire = new Date(idSplit[0]* 1000).toDateString();
                                    $.ajax({
                                        url : 'my.php',
                                        type : 'POST',
                                        dataType : 'html',
                                        data : 'id=' + storageMyFiles.items[i].id + '&dateExpire=' + dateExpire,
                                        success : function(code_html, statut){
                                            $('#myFilesTab>tbody:last').append(code_html);
                                        },
                                        error : function(resultat, statut, erreur){
                                            //console.log(resultat + statut + erreur)
                                        },
                                    });                                   
                                    var fileNotExpire=fileNotExpire+1;
                                    // On update le maintient dans le stroage
                                    newData.items.push(
                                        {id: storageMyFiles.items[i].id, key: storageMyFiles.items[i].key}
                                    );
                                } 
                            }
                            // Ecriture du storage
                            localStorage.setItem('myFiles', JSON.stringify(newData));
                        }else if (fileNotExpire == 0) {
                            $('#myFilesTab>tbody:last').append('<tr><td colspan="6" class="error myFiles0"><?= _('All your files have expired.') ?></td></tr>');
                        } else {
                            $('#myFilesTab>tbody:last').append('<tr><td colspan="6" class="error myFiles0"><?= _('No files, maybe all of them have expired.') ?></td></tr>');
                        }
                        
                    </script>
                    <?php
                    echo $echoNewUpload;
                    break;
                case '403':
                    echo '<h1><a href="'.$config['baseUrl'].'">'.$config['shortTitle'].'</a> : 403 '._('Unauthorized access').'</h1>';
                    echo $echoNewUploadPub;
                    echo '<p>'._('Unauthorized access').'</p>';
                    echo $echoNewUpload;
                    break;
                case '404':
                    echo '<h1><a href="'.$config['baseUrl'].'">'.$config['shortTitle'].'</a> : 404 '._('Not Found').'</h1>';
                    echo $echoNewUploadPub;
                    echo '<p>'._('This sharing does not exist, it has probably expired').'</p>';
                    echo $echoNewUpload;
                    break;
                default:

                    ?>
                    
                    <h1><a href="<?= $config['baseUrl'] ?>"><?= $config['shortTitle'] ?></a> : <?= $config['title'] ?></h1>
                    <?php @include_once('./start-home.php'); ?>
                    <p><?= _('Online file sharing service <a href="https://en.wikipedia.org/wiki/Open_source">free of rights</a> (license <a href="https://en.wikipedia.org/wiki/Beerware">Beerware</a>) and free.') ?></p>
                    <div id="preUpload"></div>
                    <!-- For upload results -->
                    <div id="result"></div>
                     	
                    <div class="uploadArea">
                        <?= $echoNewUpload ?>
                        <?= $similarServicesLink ?>
                    </div>
                    <?php 
                    @include_once('./end-home.php');
                }
                ?>
            <div id="footer">
                <p class="footer_right"><?= _('By') ?> <a href="http://david.mercereau.info/">David Mercereau</a> (<a href="https://framagit.org/kepon/file2link"><?= _('Git repository') ?></a>)</p>
                <p class="footer_left">file2link <?= _('version') ?> <?= VERSION ?> <?= _('is an open software licensed <a href="https://en.wikipedia.org/wiki/Beerware">Beerware</a>') ?> <span id="upgrade"></span></p>
                <?php
                if ($config['htmlPages']) {
                    echo '<p class="footer_htmlPages">';
                    foreach ($config['htmlPages'] as $fileName => $name) {
                        echo ' <a href="'.$config['baseUrl'].$fileName.'.html">'.$name.'</a> ';
                    }
                    echo '</p>';
                }
                ?>
            </div>
            <!-- Check Upgrade -->
            <?php if ($config['checkUpdate'] != false) { ?>
            <script type="text/javascript">
            function checkUpdate() {
                var timeStamp = Math.floor(Date.now() / 1000);
                if (! localStorage.getItem('checkUpdate') || Math.floor(parseInt(localStorage.getItem('checkUpdate'))+<?= $config['checkUpdate'] ?>) < timeStamp) {
                    localStorage.setItem('checkUpdate', timeStamp);
                    $.ajax({
                        url: "https://dl.zici.fr/file2link_checkupdate.php",
                        type: "GET",
                        crossDomain: true,
                        dataType: "html",
                        success: function (response) {
                            localStorage.setItem('getVersion', response);
                        },
                        error: function (xhr, status) {
                            trucAdir(3, 'Erreur dans le checkupdate' + status);
                        }
                    });
                }
            }
            checkUpdate();
            if (localStorage.getItem('getVersion')) {
                if (localStorage.getItem('getVersion').replace(/\n|\r/g,'') != '<?= VERSION ?>') {
                    $('#upgrade').html('(v' + localStorage.getItem('getVersion').replace(/\n|\r/g,'') + ' is ready, <a href="https://framagit.org/kepon/file2link">upgrade</a>)');
                }
            }
            </script>
            <?php } ?>
            <?php @include_once('./footer-page.php'); ?>
            <?php } // mod maintenance end ?>
        </div>
        <div id="bg">
            <img src="<?= $config['backgroundImage'] ?>" alt="">
        </div>       
        <!-- The template to display files available for upload -->
        <script id="template-upload" type="text/x-tmpl">
          {% for (var i=0, file; file=o.files[i]; i++) { %}
              <tr class="template-upload fade">
                  <td>
                      <span class="preview"></span>
                  </td>
                  <td>
                      {% if (window.innerWidth > 480 || !o.options.loadImageFileTypes.test(file.type)) { %}
                          <p class="name">{%=file.name%}</p>
                      {% } %}
                      <strong class="error text-danger"></strong>
                  </td>
                  <td>
                      <p class="size">Processing...</p>
                      <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
                  </td>
                  <td>
                      {% if (!o.options.autoUpload && o.options.edit && o.options.loadImageFileTypes.test(file.type)) { %}
                        <button class="btn btn-success edit" data-index="{%=i%}" disabled>
                            <i class="glyphicon glyphicon-edit"></i>
                            <span>Edit</span>
                        </button>
                      {% } %}
                      {% if (!i && !o.options.autoUpload) { %}
                          <button style="display: none" class="btn btn-primary start" disabled>
                              <i class="glyphicon glyphicon-upload"></i>
                              <span>Start</span>
                          </button>
                      {% } %}
                      {% if (!i) { %}
                          <button class="btn btn-warning cancel">
                              <i class="glyphicon glyphicon-ban-circle"></i>
                              <span><?= _('Cancel') ?></span>
                          </button>
                      {% } %}
                  </td>
              </tr>
          {% } %}
        </script>
        <!-- The template to display files available for download -->
        <script id="template-download" type="text/x-tmpl">
          {% for (var i=0, file; file=o.files[i]; i++) { %}
              <tr class="template-download fade">
                  <td>
                      <span class="preview">
                          {% if (file.thumbnailUrl) { %}
                              <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                          {% } %}
                      </span>
                  </td>
                  <td>
                      {% if (window.innerWidth > 480 || !file.thumbnailUrl) { %}
                          <p class="name">
                              {% if (file.url) { %}
                                  <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                              {% } else { %}
                                  <span>{%=file.name%}</span>
                              {% } %}
                          </p>
                      {% } %}
                      {% if (file.error) { %}
                          <div><span class="label label-danger">Error</span> {%=file.error%}</div>
                      {% } %}
                  </td>
                  <td>
                      <span class="size">{%=o.formatFileSize(file.size)%}</span>
                  </td>
                  <td>
                      <button class="btn btn-warning cancel">
                          <i class="glyphicon glyphicon-ban-circle"></i>
                          <span><?= _('Cancel') ?></span>
                      </button>
                  </td>
              </tr>
          {% } %}
        </script>
        <!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
        <script src="<?= $config['baseUrl'] ?>lib/jQuery-File-Upload/js/vendor/jquery.ui.widget.js"></script>
        <!-- The Templates plugin is included to render the upload/download listings -->
<!--
        <script src="https://blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
-->
        <script src="<?= $config['baseUrl'] ?>lib/blueimp/tmpl.min.js"></script>
        <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<!--
        <script src="https://blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
-->
        <script src="<?= $config['baseUrl'] ?>lib/blueimp/load-image.all.min.js"></script>
        <!-- The Canvas to Blob plugin is included for image resizing functionality -->
<!--
        <script src="https://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
-->
        <script src="<?= $config['baseUrl'] ?>lib/blueimp/canvas-to-blob.min.js"></script>
        <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
        <script src="<?= $config['baseUrl'] ?>lib/jQuery-File-Upload/js/jquery.iframe-transport.js"></script>
        <!-- The basic File Upload plugin -->
        <script src="<?= $config['baseUrl'] ?>lib/jQuery-File-Upload/js/jquery.fileupload.js"></script>
        <!-- The File Upload processing plugin -->
        <script src="<?= $config['baseUrl'] ?>lib/jQuery-File-Upload/js/jquery.fileupload-process.js"></script>
        <!-- The File Upload image preview & resize plugin -->
        <script src="<?= $config['baseUrl'] ?>lib/jQuery-File-Upload/js/jquery.fileupload-image.js"></script>
        <!-- The File Upload audio preview plugin -->
        <script src="<?= $config['baseUrl'] ?>lib/jQuery-File-Upload/js/jquery.fileupload-audio.js"></script>
        <!-- The File Upload video preview plugin -->
        <script src="<?= $config['baseUrl'] ?>lib/jQuery-File-Upload/js/jquery.fileupload-video.js"></script>
        <!-- The File Upload validation plugin -->
        <script src="<?= $config['baseUrl'] ?>lib/jQuery-File-Upload/js/jquery.fileupload-validate.js"></script>
        <!-- The File Upload user interface plugin -->
        <script src="<?= $config['baseUrl'] ?>lib/jQuery-File-Upload/js/jquery.fileupload-ui.js"></script>
        <script type="text/javascript">

            $('#logout').on( "click", function() {
                window.location.replace("/?log=out")
            });

            function convertHumain2octect(value) {
                var regexKB = RegExp('KB$');
                var regexMB = RegExp('MB$');
                var regexGB = RegExp('GB$');
                if (regexKB.test(value)) {
                    return parseFloat(fileSize)*1024;
                }else if (regexMB.test(value)) {
                    return parseFloat(fileSize)*1024*1024;
                }else if (regexGB.test(value)) {
                    return parseFloat(fileSize)*1024*1024*1024;
                }
            }
            function convertOctect2Humain(value) {
                if (value > 1000000000) {
                    return (value/1024/1024/1024).toFixed(2) + ' Go';
                }else if (value > 1000000) {
                    return (value/1024/1024).toFixed(2) + ' Mo';
                }else if (value > 1000) {
                    return (value/1024).toFixed(2) + ' Ko';
                } else {
                    return value;
                }
            }
           

            function checkQuotaFiles() {
                <?php 
                if ($action == 'html') {
                    $fileAlreadyUploadSizeTotal=0;
                    $uploadDir=$config['uploadDir'].'/'.$id.'/';
                    foreach (scandir($uploadDir) as $fileAlreadyUpload) {
                        if (is_file($uploadDir.$fileAlreadyUpload)) {
                            $fileAlreadyUploadSizeTotal=filesize($uploadDir.$fileAlreadyUpload)+$fileAlreadyUploadSizeTotal;
                        }
                    }
                    echo 'var totalSize='.$fileAlreadyUploadSizeTotal.';';
                } else {
                    echo 'var totalSize=0;';
                }
                ?>
                //~ console.log(totalSize);
                findUploadFiles=$('#fileupload').find('.size').get();
                for(var i = 0; i< findUploadFiles.length; i++){
                    fileSize=findUploadFiles[i].innerText;
                    totalSize=totalSize+convertHumain2octect(fileSize);
                }
                if (totalSize > <?= convertHumain2octect($config['maxUploadTotal']) ?>) {
                    $('#maxUploadTotalError').show();
                    $('#ButtonAdd').hide();
                    $('#ButtonStart').hide();
                } else {
                    $('#maxUploadTotalError').hide();
                    $('#ButtonAdd').show();
                    $('#ButtonStart').show();
                }
                if (findUploadFiles.length > 0) {
                    $('#ButtonReset').show();
                } else {
                    $('#ButtonReset').hide();
                    $('#ButtonStart').hide();
                }
                //~ console.log(totalSize);
                return true;
            }
            
            
            $('#fileupload').fileupload({
                url: '<?= $config['baseUrl']?>/upload.php',
                added: function (e) {
                    checkQuotaFiles();
                },
                started: function (e) {
                    console.log('started');
                    $('.shareUrlPrint').show();
                    $('.expire-button').hide();
                    $('#uploadOptionsLinkShow').hide();
                    $('#uploadOptions').hide();
                    $('#ButtonStart').hide();
                    $('#ButtonMyFiles').hide();
                    $('#ButtonAdd').hide();
                },
                // Se déclenche 
                finished: function (e, data) {
                    // Si ce n'est pas une annulatoin mais bien une fin d'upload
                    if (data.errorThrown != 'abort') {
                        // On mémorise l'upload dans le localStorage
                        if (localStorage.getItem('myFiles')) {
                            var data = JSON.parse(localStorage.getItem('myFiles'));
                        } else {
                            var data = {items: []};
                        }
                        // S'il n'existe pas déjà
                        if (data.items.filter(function(e) { return e.id === $('#files_id').val(); }).length == 0) {
                            data.items.push(
                                {id: $('#files_id').val(), key: $('#files_key').val()}
                            );
                            localStorage.setItem('myFiles', JSON.stringify(data));
                        }
                    }
                },
                failed: function (e, data) {
                    console.log(data);
                    console.log(e);
                    // Si un upload est en cours
                    if (data.loaded > 0) {
                        //~ console.log(data);
                        //~ location.href='<?= $config['baseUrl']?>/'+$('#files_id').val()+'/';
                        $('#files_id').val(idGen());
                        $('#shareUrl').val('<?= $config['baseUrl'] ?>/' + $('#files_id').val() + '/');
                        var keyGen = Math.floor(Math.random() * (999999999999 - 100000000000) + 100000000000);
                        $('#files_key').val(keyGen);
                    } else {
                        checkQuotaFiles();
                    }
                }
                
            });

            // Enable iframe cross-domain access via redirect option:
            //~ $('#fileupload').fileupload(
                //~ 'option',
                //~ 'redirect',
                //~ window.location.href.replace(/\/[^/]*$/, 'lib/jQuery-File-Upload-master//cors/result.html?%s')
            //~ );

            function redirectToFiles() {
                location.replace('<?= $config['baseUrl']?>/'+ $('#files_id').val() + '/');
            }

            $('#fileupload').fileupload('option', {
                    // Enable image resizing, except for Android and Opera,
                    // which actually support image resizing, but fail to
                    // send Blob objects via XHR requests:
                    disableImageResize: /Android(?!.*Chrome)|Opera/.test(
                    window.navigator.userAgent
                ),
                imageMaxWidth: $('#resize').val(),
                imageMaxHeight:  $('#resize').val(),
                maxFileSize: <?= convertHumain2octect($config['maxUploadPerFile']) ?>, 
                minFileSize: <?= convertHumain2octect($config['minUploadPerFile']) ?>, 
                maxNumberOfFiles: <?= $config['maxUploadNb'] ?>, 
                sequentialUploads: true,
                limitConcurrentUploads: 1,    // To limit the number of concurrent uploads, set this option to an integer value greater than 0.
                acceptFileTypes: <?= $config['acceptFileTypes'] ?>
                
            }).on('fileuploadprogressall', function (e, data) {
                if (data.loaded == data.total) {
                    $('#ButtonReset').hide();
                    $('.btn.btn-warning.cancel').hide();
                    $('#redirectToFiles').show();
                    $('.redirectToFilesId').text($('#files_id').val());
                    setTimeout(redirectToFiles, 1000);
                }
            });

            <?php if ($action == 'html') { ?>
                $('#files_id').val('<?= $id ?>');
            <?php } else { ?>
                $('#files_id').val(idGen());
                var keyGen = Math.floor(Math.random() * (999999999999 - 100000000000) + 100000000000);
                $('#files_key').val(keyGen);
            <?php } ?>
            $('#shareUrl').val('<?= $config['baseUrl'] ?>/' + $('#files_id').val() + '/');

        </script>
        <!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
        <!--[if (gte IE 8)&(lt IE 10)]>
          <script src="js/cors/jquery.xdr-transport.js"></script>
        <![endif]--> 
    </body>
</html>
<?php 
if ($config['expireCron'] == 'web') {
    if (is_file($config['uploadDir'].'/.cronWeb')) {
        if (file_get_contents($config['uploadDir'].'/.cronWeb')+$config['expireCronFreq'] < time()) {
            cronExpire();
            file_put_contents($config['uploadDir'].'/.cronWeb', time());
        }
    } else {
        file_put_contents($config['uploadDir'].'/.cronWeb', time());
    }
}
@include_once('./footer.php'); 

?>

