<?php

if (!isset($_POST['id']) or !isset($_POST['dateExpire'])) {
    exit('No hack 1');
}
$id = $_POST['id'];
if (!preg_match('/^[0-9]+-[0-9]{1,2}$/', $id)) {
    exit('No Hack ID');
}

$idSplit = explode('-', $id);

$config = yaml_parse_file('./config.yaml'); 
include('./lib/functions.php');

$uploadDirId=$config['uploadDir'].'/'.$id;

if (is_dir($uploadDirId)) {
    $nbFile=0;
    $contentType = null;
    $contentTypeMultiple = false;
    $filesSize=0;
    foreach (scandir($uploadDirId) as $file) {
        if (is_file($uploadDirId.'/'.$file) 
        && $file != $id.'.zip'
        && !preg_match('/^\.(.+)\.cfg$/', $file)
        && !preg_match('/^\.(.+)\.small$/', $file)) {
            $nbFile++;
            $filesSize=filesize($uploadDirId.'/'.$file)+$filesSize;
            $contentTypeNew=mime_content_type($uploadDirId.'/'.$file);
            if ($contentType == null) { 
            $contentType = $contentTypeNew;
            } elseif ($contentType != $contentTypeNew) {
            $contentTypeMultiple = true;
            }
        }
    }

    echo '<tr onclick="location.href=\''.$config['baseUrl'].'/'.$id.'/'.'\'">';
    if ($contentTypeMultiple) {
        echo '<td><div class="fi fi-size-xs fi-any">
            <div class="fi-content">'._('Multi').'</div>
        </div></td>';
    } else {
        $contentTypeSplit = explode('/', $contentType);
        echo '<td><div class="fi fi-size-xs fi-'.$contentTypeSplit[1].'">
            <div class="fi-content">'.$contentTypeSplit[1].'</div>
        </div></td>';
    }
    echo '<td>'.$nbFile.'</td>';
    echo '<td>'.date("Y/m/d", filectime($uploadDirId)).'</td>';
    echo '<td>'.date("Y/m/d", $idSplit[0]).'</td>';
    echo '<td>'.convertOctect2humain($filesSize).'</td>';
    if (is_file($uploadDirId.'/.access.cfg') ) {
    	echo '<td>'.file_get_contents($uploadDirId.'/.access.cfg').'</td>';
    }else{
	    echo '<td>'._('Unlimited').'</td>';
    }
    if (is_file($uploadDirId.'/.password.cfg') ) {
    	echo '<td> '._('Yes').' </td>';
    }else{
	    echo '<td> '._('No').' </td>';
    }
    echo '<td><a href="'.$config['baseUrl'].'/'.$id.'/'.'">'._('Link').'</a></td>';
    echo '</tr>';

}
?>
