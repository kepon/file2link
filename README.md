# file2link 
PHP file sharing service [free of rights](https://en.wikipedia.org/wiki/Open_source) (license [Beerware](https://en.wikipedia.org/wiki/Beerware)) and free. 

**Demonstration : https://dl.zici.fr/**

## Requis

 - HTTP serveur htaccess compatible (url rewriting enable)
 - PHP 5.6 minimum (PHP 8 tested ok)
 - PHP GD lib 
    - ImageMagick convert binary (option)
 - PHP PDO sqlite (no requir if check_checksum:false)
 - PHP IMAP  (no requir if authImap:false)
 - No nessecary database

## Installation

Upload source content on you web server

Move config file : 

    mv config.yaml.exemple config.yaml

Change paramètre in config.yaml Make sure the consistency between:
 - *maxUploadPerFile* is similar to PHP config *upload_max_filesize* and *post_max_size* 
 - *maxUploadNb* is similar to PHP config *max_file_uploads*

Make sure the "files" directory is writable

### Crontab

By default the expired files this deletes with visits to the homepage. But you can delete them by cron job:

    10	0	*	*	*	cd /var/www/dl.zici.fr/ && php /var/www/dl.zici.fr/cron.php

In config.yaml change

    expireCron: web

to 

    expireCron: cli

### Security

https://github.com/blueimp/jQuery-File-Upload/blob/master/SECURITY.md#secure-file-upload-serving-configurations

## Changelog

 - futur
 	- jQuery-File-Upload is deprecated
	- serveur disque use limit (check % space and disable upload if < X%)
	- drag and drop for upload
	- limit by ip+cookies (sqlite) (limit abuse)
	- crypt files
	- captcha / anti bot
	- "signaler un fichier"
	- mimeTypes restriction
 - 1.2
	- Authentification IMAP for upload
	- Authentification "gest" (single password) for upload
 - 1.1
	- No upload file if exist on server, just link
		- required PDO sqlite
```
check_checksum: true
checksumDb: files/checksum.db
```
 - 1.0
    - jQuery-File-Upload implemented : https://github.com/blueimp/jQuery-File-Upload/
    - Resize image befor upload
    - Preview audio/video/image befor upload
    - Config add variable : 
		- minUploadPerFile, acceptFileTypes, imageAutoOrient
		- Deprecated : mimeTypes restriction
	- Add on existent sharing
 - 0.2
	- Mod gallery if images
	- Passowrd for access
	- Delete after first access OR define nb access (10, 20...)
	- Bug Fix : 
	  - BUG day / days
	  - BUG my files no work in html page
	  - BUG delete file, but already in storage
	- MyFiles Rewrite
 - 0.1 (16/10/2019)
	 - Configure with YAML file
	 - Submit a file upload service with temporary link (Direct link, force download link, download multiple after zip)
	 - Limit in time
	 - Limit in number is possible
	 - Limit on type of file (mime) is possible
	 - Upload with progress bar in HTML5
	 - Upload multi file
	 - Multilingual (French / English)
	 - Images resize possible
	 - Find these files before uploader (local storage)
	 - Housekeeping in files expired by cron job or by web traffic on the page
	 - Personnalize content page with include header-page.php, header.php, footer-page.php, footer.php, start-home.php, end-home.php

# Monitoring with munin

```bash
cd munin 
cp file2link_* /etc/munin/plugins/
```

On /etc/munin/plugin-conf.d/munin-node file, add your "files" dir :

```ini
 [...]
 
 [file2link_*]
     env.dir /var/www/dl.zici.fr/files
```

And restart munin node

```bash
/etc/init.d/munin-node restart
```

# Licence

Beerware : https://en.wikipedia.org/wiki/Beerware

# Crédit

Mime type icone : https://github.com/colorswall/CSS-file-icons  
Icone : https://fontawesome.com   Creative Commons Attribution 4.0 International license

# Author

David Mercereau [david #arobase# mercereau #point# info](http://david.mercereau.info/contact/) 

## Translation

* oc : Quentin PAGÈS 
  * fr/en : David Mercereau
