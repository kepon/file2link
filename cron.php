#!/usr/bin/php
<?php
$config = yaml_parse_file('./config.yaml'); 
include('./lib/functions.php');

if ($config['expireCron'] == 'cli') {
    if (!is_dir($config['uploadDir'])) {
        exit(_('Start the command in the file2link directory'));
    }
    cronExpire();
    echo _('Completed')."\n";
} else {
    exit(_('expireCron is not at "cli" mod (in config.yaml)'));
}



?>
