<?php




if (!isset($_POST['expire']) or !isset($_POST['id']) or !isset($_POST['key'])) {
   exit('No hack 1');
}

$config = yaml_parse_file('./config.yaml'); 
include('./lib/functions.php');

// # False = pas de checksum
// var_dump(md5_file('files/checksum.db'));
// var_dump(md5_file('checksum.db'));


// require('lib/Checksum.php');

// $checksum = new Checksum();

// #var_dump($checksum->addFile("12121353-12", "toto.txt", "0000000001", 1681907411));

// #var_dump($checksum->checkChecksum("0000000000"));
// var_dump($checksum->checkChecksum("0000000001"));
// var_dump($checksum->cleanExpire());
// var_dump($checksum->checkChecksum("0000000000"));
// #var_dump($checksum->deleteFile("12121353-12", 'toto.txt'));

// #var_dump($checksum->checkChecksum("0000000000"));

// exit();

if (isset($_COOKIE['langue'])) {
	$locale = lang2locale($_COOKIE['langue']);
	$localeshort=locale2lang($locale);
} else {
	$HTTP_ACCEPT_LANGUAGE=$_SERVER['HTTP_ACCEPT_LANGUAGE'];
	//echo $HTTP_ACCEPT_LANGUAGE.'<br />';
	$lang_from_http_accept = explode(',', $HTTP_ACCEPT_LANGUAGE);
	//echo $lang_from_http_accept[0].'<br />';
	$locale = lang2locale($lang_from_http_accept[0]);
	if (substr($locale,0,2) != substr($lang_from_http_accept[0],0,2)) {
		//echo "Non trouvé, 2ème tentative";
		$lang_from_http_accept = explode('-', $lang_from_http_accept[0]);
		//echo $lang_from_http_accept[0].'<br />';
		$locale = lang2locale($lang_from_http_accept[0]);
	}
	//echo $locale.'<br />';
	$localeshort=locale2lang($locale);
}
// Définition de la langue :
$results=putenv("LC_ALL=$locale.utf8");
if (!$results) {
    exit ('putenv failed');
}
$results=putenv("LC_LANG=$locale.utf8");
if (!$results) {
    exit ('putenv failed');
}
$results=putenv("LC_LANGUAGE=$locale.utf8");
if (!$results) {
    exit ('putenv failed');
}
$results=setlocale(LC_ALL, "$locale.utf8");
if (!$results) {
    exit ('setlocale failed: locale function is not available on this platform, or the given local does not exist in this environment');
}
bindtextdomain("messages", "./lang");
textdomain("messages");


$id=$_POST['id'];
$uploadDir = $config['uploadDir'].'/'.$id;
// Création du répertoire
if (!is_dir($uploadDir)) {
    mkdir($uploadDir);
} else {
    $fileAlreadyUploadSizeTotal=0;
    foreach (scandir($uploadDir) as $fileAlreadyUpload) {
        if (is_file($uploadDir.'/'.$fileAlreadyUpload)) {
            $fileAlreadyUploadSizeTotal=filesize($uploadDir.'/'.$fileAlreadyUpload)+$fileAlreadyUploadSizeTotal;
        }
    }
}

if (!is_file($uploadDir.'/.key-'.$_POST['key'].'.cfg')) {
    touch($uploadDir.'/.key-'.$_POST['key'].'.cfg');
}

if (isset($_POST['accessCheckbox']) && isset($_POST['access']) && preg_match('/^[0-9]+$/', $_POST['access']))  {
    if (!is_file($uploadDir.'/.access.cfg')) {
		file_put_contents($uploadDir.'/.access.cfg', $_POST['access']);
    }
}

if (isset($_POST['passwordCheckbox']) && isset($_POST['password']))  {
	file_put_contents("/tmp/pwd", $_POST['password']);
    if (!is_file($uploadDir.'/.password.cfg')) {
		file_put_contents($uploadDir.'/.password.cfg', password_hash($config['passwordUniqKey'].$_POST['password'], PASSWORD_DEFAULT));
    }
}


require('lib/UploadHandler.php');
$upload_handler = new UploadHandler();
