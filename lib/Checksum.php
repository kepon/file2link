<?php
class Checksum{
    private $config;
    private $dbco;
    // Récupération de la config
    public function __construct(){
		global $config;
        $this->config = $config;
        // # Sans effet
        // if ($this->config->check_checksum !== true) {
        //     return false;
        // }
        // Connect DB
        try {
            $this->dbco = new PDO('sqlite:'.$this->config['checksumDb']);
            $this->dbco->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch ( PDOException $e ) {
            die("Connexion à la base '".$this->config['checksumDb']."' : ".$e->getMessage());
        }
        // Create DB if not exists
        try {
            $create = $this->dbco->query("
                CREATE TABLE IF NOT EXISTS checksum (
                id INTEGER PRIMARY KEY,
                file_path CHAR(250) NOT NULL UNIQUE,
                file_md5sum CHAR(150) NOT NULL,
                dateExpir INTEGER NOT NULL);
            ");
        } catch ( PDOException $e ) {
            echo "Error initializing checksum tables : ".$e;
            die();
        }
	}

    function cleanExpire() {
        try {
            $deletecmd = $this->dbco->prepare("DELETE FROM checksum WHERE  dateExpir < ".time());
            $deletecmd->execute();
        } catch (PDOException $e) {
            return ['error' => $e->getMessage()];
        }
        return true;
    }

    function deleteFile($file_path) {
        try {
            $deletecmd = $this->dbco->prepare("DELETE FROM checksum WHERE file_path = :file_path");
            $deletecmd->bindParam('file_path', $file_path, PDO::PARAM_STR);
            $deletecmd->execute();
        } catch (PDOException $e) {
            return ['error' => $e->getMessage()];
        }
        return true;
    }

    function addFile($file_path, $file_md5sum, $dateExpir) {
        try {
            $insertcmd = $this->dbco->prepare("INSERT INTO checksum (file_path, file_md5sum, dateExpir) 
                                            VALUES (:file_path, :file_md5sum, :dateExpir)");
            $insertcmd->bindParam('file_path', $file_path, PDO::PARAM_STR);
            $insertcmd->bindParam('file_md5sum', $file_md5sum, PDO::PARAM_STR);
            $insertcmd->bindParam('dateExpir', $dateExpir, PDO::PARAM_INT);
            $insertcmd->execute();
        } catch (PDOException $e) {
            return ['error' => $e->getMessage()];
        }
        return true;
    }

    function checkChecksum($checksum) {
        if ($this->config['check_checksum'] != true) {
            return false;
        }
        try {
            $selectcmd = $this->dbco->prepare("SELECT id, file_path, dateExpir FROM checksum
                                                    WHERE file_md5sum = :file_md5sum
                                                    LIMIT 1");
            $selectcmd->bindParam('file_md5sum', $checksum, PDO::PARAM_STR);
            $selectcmd->execute();
            return $selectcmd->fetch();
        } catch (PDOException $e) {
            return ['error' => $e->getMessage()];
        }
    }   
}
?>