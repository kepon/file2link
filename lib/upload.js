
function idGen() {
    var expire = Math.floor(Date.now() / 1000 + $('#expire').val() * 86400);
    var random = Math.floor(Math.random() * Math.floor(99));
    return expire + '-' + random;
}

$(function () {    
    // Copy on clipart : https://stackoverflow.com/questions/44888884/copying-to-clipboard-textbox-value-using-jquery-javascript
    function copyToClipboard(text) {
       var textArea = document.createElement( "textarea" );
       textArea.value = text;
       document.body.appendChild( textArea );
       textArea.select();
       try {
	      var successful = document.execCommand( 'copy' );
	      var msg = successful ? 'successful' : 'unsuccessful';
       } catch (err) {
	      console.log('Oops, unable to copy');
       }
       document.body.removeChild( textArea );
    }
    
    

    $( "#similarServices" ).click(function() {
	    $('.similarHref').hide();	
	    $('.similarLink').show();	
    });

    $(document).on('click', '.copy', function(){
	    copyToClipboard($(this).val());
	    $(this).select();
    });

    $("#passwordCheckbox").on('change',function(){
	    if( $('#passwordCheckbox').is(':checked') ){
		    $('#passwordForm').show();
	    } else {
		    $('#passwordForm').hide();
	    }
    });

    $("#accessCheckbox").on('change',function(){
	    if( $('#accessCheckbox').is(':checked') ){
		    $('#accessForm').show();
	    } else {
		    $('#accessForm').hide();
	    }
    });
    
    $("#uploadOptionsLinkShow").on('click',function(){
	    $('#uploadOptions').show();
	    $('#uploadOptionsLinkShow').hide();
    });
    
    $("#uploadOptionsLinkHide").on('click',function(){
	    $('#uploadOptions').hide();
	    $('#uploadOptionsLinkShow').show();
    });
    
    $("#expire").on('change',function(){
	$('#files_id').val(idGen());
    });   
       
    $("#resize").on('change',function(){
	$('#fileupload').fileupload('option', {
	    imageMaxWidth: $('#resize').val(),
	    imageMaxHeight:  $('#resize').val(),
	});
    });       

    $('#ButtonStart').hide();
    $('#ButtonReset').hide();
	
});

