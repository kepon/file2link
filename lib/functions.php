<?php 

require_once('Checksum.php');
require_once('lib/Checksum.php');
require_once('Auth.php');
require_once('lib/Auth.php');

//							CODE			LOCALE (locale -a)
$langueEtLocalDispo=array(	'fr'		=> 'fr_FR', 
							'en'		=> 'en_US',
							'oc'		=> 'oc_FR',
							);

function convertHumain2octect($value) {
	if (preg_match('/[0-9]+[Kk]$/', $value)) {
		return intval($value) * 1024;
	} elseif (preg_match('/[0-9]+[Mm]$/', $value)) {
		return intval($value) * 1024 * 1024;
	} elseif (preg_match('/[0-9]+[Gg]$/', $value)) {
		return intval($value) * 1024 * 1024 * 1024;
	} else {
        return intval($value);
    }
}
function convertOctect2humain($value) {
	if ($value > 1000000000) {
		$return=round($value/1024/1024/1024, 1).'Go';
	}elseif ($value > 1000000) {
		$return=round($value/1024/1024, 1).'Mo';
	}elseif ($value > 1000) {
		$return=round($value/1024, 1).'Ko';
	} else {
		$return=$value;
	}
	return $return;
}


function checkMimeTypes($mimeTypesTest) {
    global $config;
    $mimeDetect=false;
    foreach ($config['mimeTypes'] as $mimeTypes) {
        if (preg_match('/'.$mimeTypes.'/', $mimeTypesTest)) {
            $mimeDetect = true;
        }
    }
    if (($config['mimeTypesConduct'] == 'allow' && $mimeDetect)  
    || ($config['mimeTypesConduct'] == 'deny'  && !$mimeDetect)) {
        return true;
    } else {
        return false;
    }
    return $return;
}


function genZip($id) {
    global $config;
    $uploadDirId=$config['uploadDir'].'/'.$id;
    $zipFile = $uploadDirId.'/'.$id.'.zip';
    if (!is_file($zipFile)) {
        $zip = new ZipArchive();
        if ($zip->open($zipFile, ZipArchive::CREATE)!==TRUE) {
            exit('Error in open <$zipFile>\n');
        }
        foreach (scandir($uploadDirId) as $file) {
            if (is_file($uploadDirId.'/'.$file) 
				&& !preg_match('/^\.(.+)\.cfg$/', $file)
				&& !preg_match('/^\.(.+)\.small$/', $file)) {
					$zip->addFile($uploadDirId.'/'.$file,$file);
            }
        }
        $zip->close();
    }
}

function rrmdir($dir) { 
	$checksum = new Checksum;
	if (is_dir($dir)) { 
		$objects = scandir($dir); 
		foreach ($objects as $object) { 
			if ($object != "." && $object != "..") { 
				if (is_dir($dir."/".$object) && !is_link($dir."/".$object)) {
					rrmdir($dir."/".$object);
				} else {
					unlink($dir."/".$object); 
					//error_log("deleteFile : ".json_encode($dir."/".$object));
					$checksumDeleteFile = $checksum->deleteFile($dir."/".$object);
					//error_log("checksumDeleteFile : ".json_encode($checksumDeleteFile));
					if ($checksumDeleteFile != true) {
						exit("checksumDeleteFile : ".json_encode($checksumDeleteFile));
					}
				}
			} 
		}
		rmdir($dir); 
	} 
}

function cronExpire() {
	global $config;
	foreach (scandir($config['uploadDir']) as $uploadDirId) {
		if (!preg_match('/^\./', $uploadDirId) && is_dir($config['uploadDir'].'/'.$uploadDirId)) {
			$uploadDirIdExplode = explode("-", $uploadDirId);
			if ($uploadDirIdExplode[0] < time()) {
				if ($config['expireCron'] == 'cli') {
					echo $config['uploadDir'].'/'.$uploadDirId." "._('Expired')."\n";
				}
				rrmdir($config['uploadDir'].'/'.$uploadDirId);
			}
		}
	}
}

// https://www.binarytides.com/php-resize-large-images-imagemagick/
function resize_image($src , $dest , $toWidth , $toHeight )  {
	if(!file_exists($src)) {
		echo '$src file does not exist';
		return false;
	} else {
		
		//OPEN THE IMAGE INTO A RESOURCE
		$img = imagecreatefromjpeg ($src);	//try jpg
		if(!$img) {
			$img = imagecreatefromgif ($src);	//try gif
		}
		if(!$img) {
			$img = imagecreatefrompng ($src);	//try png
		}
		if(!$img) {
			die('Could Not create image resource $src');
		}
		
		//ORIGINAL DIMENTIONS
		list( $width , $height ) = getimagesize( $src );
		
		//ORIGINAL SCALE
		$xscale=$width/$toWidth;
		$yscale=$height/$toHeight;
		
		//NEW DIMENSIONS WITH SAME SCALE
		if ($yscale > $xscale) {
			$new_width = round($width * (1/$yscale));
			$new_height = round($height * (1/$yscale));
		} else {
			$new_width = round($width * (1/$xscale));
			$new_height = round($height * (1/$xscale));
		}
		
		//NEW IMAGE RESOURCE
		if(!($imageResized = imagecreatetruecolor($new_width, $new_height))) {
			die('Could not create new image resource of width : $new_width , height : $new_height');
		}
		
		//RESIZE IMAGE
		if(! imagecopyresampled($imageResized, $img , 0 , 0 , 0 , 0 , $new_width , $new_height , $width , $height)) {
			die('Resampling failed');
		}
		
		//STORE IMAGE INTO DESTINATION
		if(! imagejpeg($imageResized , $dest)) {
			die('Could not save new file');
		}
		
		//Free the memory
		imagedestroy($img);
		imagedestroy($imageResized);
		
		return true;
	} 
}

function truncate($string, $max_length = 30, $replacement = '', $trunc_at_space = false) {
	$max_length -= strlen($replacement);
	$string_length = strlen($string);
	
	if($string_length <= $max_length)
		return $string;
	
	if( $trunc_at_space && ($space_position = strrpos($string, ' ', $max_length-$string_length)) )
		$max_length = $space_position;
	
	return substr_replace($string, $replacement, $max_length);
}




function lang2locale($langue) {
	global $langueEtLocalDispo;
	if ($langueEtLocalDispo[$langue] != '') {
		return $langueEtLocalDispo[$langue];
	} else {
		// par défaut
		return 'en_US';
	}
}
function locale2lang($localeRecherche) {
	global $langueEtLocalDispo;
	foreach($langueEtLocalDispo as $code=>$locale) {
		if ($locale == $localeRecherche) {
			return $code; 
			break;
		}
	}
	// par défaut
	return 'en';
}

// Get IP
function GetIp()
{
    $IP = false;
    if (getenv('HTTP_CLIENT_IP')) {
        $IP = getenv('HTTP_CLIENT_IP');
    } else if(getenv('HTTP_X_FORWARDED_FOR')) {
        $IP = getenv('HTTP_X_FORWARDED_FOR');
    } else if(getenv('HTTP_X_FORWARDED')) {
        $IP = getenv('HTTP_X_FORWARDED');
    } else if(getenv('HTTP_FORWARDED_FOR')) {
        $IP = getenv('HTTP_FORWARDED_FOR');
    } else if(getenv('HTTP_FORWARDED')) {
        $IP = getenv('HTTP_FORWARDED');
    } else if(getenv('REMOTE_ADDR')) {
        $IP = getenv('REMOTE_ADDR');
    }

    //If HTTP_X_FORWARDED_FOR == server ip
    if((($IP) && ($IP == getenv('SERVER_ADDR')) && (getenv('REMOTE_ADDR')) || (!filter_var($IP, FILTER_VALIDATE_IP)))) {
        $IP = getenv('REMOTE_ADDR');
    }

    if($IP) {
        if(!filter_var($IP, FILTER_VALIDATE_IP)) {
            $IP = false;
        }
    } else {
        $IP = false;
    }
    return $IP;
}
?>
