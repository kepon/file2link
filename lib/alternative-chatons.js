const jsonFile = "https://www.chatons.org/entraide/json";
const typeFiltre = "Partage temporaire de fichiers (alternative à WeTransfer, DropSend)"
const openFiltre = "Ouvert à tou⋅te⋅s sans inscription"
const separateur = ' - '
//~ console.log('alternative chatons');
// onready : Inspiré : http://jsfiddle.net/electricvisions/Jacck/
document.onreadystatechange = function () {
    var state = document.readyState
    // A la fin du chargement du document 
    if (state == 'complete') {
	entraide = {};
	// start JSON retrieval here
	$.getJSON(jsonFile, function(data) {
	    entraide = data; 
	    var x=0;
	    for (var i = 0; i < entraide.length; i++) {
		if (entraide[i].type == "Partage temporaire de fichiers" 
		&& entraide[i].access_type == "Ouvert à tou⋅te⋅s") {
		    if (x != 0) {
			$("#entraide-chatons-alternative").append(separateur);	    
		    }
		    link=entraide[i].endpoint;
		    link=link.replace('https://', '');
		    link=link.replace('http://', '');
		    //console.log(link);	
		    const regex = /\/$/i;
		    link = link.replace(regex, '')
		    $("#entraide-chatons-alternative").append('<a href="'+entraide[i].endpoint+'">'+link+'</a>');
		    x=x+1;
		}
	    }
	});
    }
}

