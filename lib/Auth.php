<?php

class Auth{
    private $config;
    private $dbco;
    // Récupération de la config
    public function __construct(){
		global $config;
        $this->config = $config;

        if (! function_exists('imap_open') && $this->config['authImap'] ==  true) {
            exit("IMAP functions are not available, but authImap = true in your config. Install IMAP php extension");
        }

        // # Sans effet
        // if ($this->config->check_checksum !== true) {
        //     return false;
        // }
        // Connect DB
        try {
            $this->dbco = new PDO('sqlite:'.$this->config['authDb']);
            $this->dbco->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch ( PDOException $e ) {
            die("Connexion à la base '".$this->config['authDb']."' : ".$e->getMessage());
        }
        // Create DB if not exists
        try {
            $create = $this->dbco->query("
                CREATE TABLE IF NOT EXISTS auth (
                id INTEGER PRIMARY KEY,
                who CHAR(250) NOT NULL,
                ip CHAR(128) NOT NULL,
                fingerprinting CHAR(32) NOT NULL,
                token CHAR(150) NOT NULL,
                dateExpire INTEGER NOT NULL);
            ");
        } catch ( PDOException $e ) {
            echo "Error initializing auth tables : ".$e;
            die();
        }
	}

    // L'emprunte
    function fingerprinting() {
        return md5($_SERVER['HTTP_USER_AGENT'].GetIp());
    }

    // Expirer les logins
    function cleanExpire() {
        try {
            $deletecmd = $this->dbco->prepare("DELETE FROM auth WHERE  dateExpire < ".time());
            $deletecmd->execute();
        } catch (PDOException $e) {
            return ['error' => $e->getMessage()];
        }
        return true;
    }

    // Logout
    function logout($token) {
        try {
            $deletecmd = $this->dbco->prepare("DELETE FROM auth WHERE token = :token");
            $deletecmd->bindParam('token', $token, PDO::PARAM_STR);
            $deletecmd->execute();
        } catch (PDOException $e) {
            return ['error' => $e->getMessage()];
        }
        return true;
    }

    /*
    Enregistrement du login dans la base
    Param : 
        - $who : Qui (e-mail)
        - $expire: en heure
    */
    private function loginRecord($who, $expire) {
        $dateExpire = time()+$expire*60*60;
        $fingerprinting = $this->fingerprinting();
        $token=md5($who.$dateExpire.$fingerprinting.rand());
        $ip = GetIp();
        try {
            $insertcmd = $this->dbco->prepare("INSERT INTO auth (token, who, ip, fingerprinting, dateExpire) 
                                            VALUES (:token, :who, :ip, :fingerprinting, :dateExpire)");
            $insertcmd->bindParam('token', $token, PDO::PARAM_STR);
            $insertcmd->bindParam('who', $who, PDO::PARAM_STR);
            $insertcmd->bindParam('ip', $ip, PDO::PARAM_STR);
            $insertcmd->bindParam('fingerprinting', $fingerprinting, PDO::PARAM_STR);
            $insertcmd->bindParam('dateExpire', $dateExpire, PDO::PARAM_INT);
            $insertcmd->execute();
        } catch (PDOException $e) {
            return ['error' => $e->getMessage()];
        }
        return $token;
    }

    // Authentification IMAP
    private function loginImap($login, $password, $expire) {
        if ($mbox=@imap_open($this->config['authImapOpenMailbox'], $login, $password, OP_READONLY, 0)) {
            if ($loginRecordReturn = $this->loginRecord($login, $expire)) {
                return ['token' => $loginRecordReturn];
            } else {
                return ['error' => "loginRecord Error : ".$loginRecordReturn];
            }
            imap_close($mbox);
        } else {
            return false;
        }        
    }

    // Authentification Guest
    private function loginGuest($password, $expire) {
        if (password_verify($password, $this->config['authGuestSinglePassword'])) {
            if ($loginRecordReturn = $this->loginRecord('guest', $expire)) {
                return ['token' => $loginRecordReturn];
            } else {
                return ['error' => "loginRecord Error : ".$loginRecordReturn];
            }
            return true; 
        } else {
            return false;
        }
    }

    // Login routing
    function login($login, $password, $type, $expire) {
        if (!array_key_exists($expire, $this->config['authExpire'])) {
            exit('No hack : authExpire');
        }
        if ($type == 'imap' && $this->config['authImap'] ==  true) {
            return $this->loginImap($login, $password, $expire);
        } elseif ($type == 'guest' && $this->config['authGuest'] ==  true) {
            return $this->loginGuest($password, $expire);
        } else {
            return ['error' => 'Login error, check config.yml'];
        }
    }

    // Check if token exist
    function tokenExist($token) {
        // Ménage dans les token expiré
        $this->cleanExpire();
        // Constitution de l'emprunte
        $fingerprinting = $this->fingerprinting();
        // Recherche de l'existence d'un token
        try {
            $selectcmd = $this->dbco->prepare("SELECT COUNT(who) FROM auth
                                                WHERE fingerprinting = :fingerprinting
                                                AND token = :token
                                                AND dateExpire > ".time()."
                                                LIMIT 1");
            $selectcmd->bindParam('token', $token, PDO::PARAM_STR);
            $selectcmd->bindParam('fingerprinting', $fingerprinting, PDO::PARAM_STR);
            $selectcmd->execute();
            $fetch = $selectcmd->fetch();
            if ($fetch[0] != 1) {
                return false;
            } else {   
                return true;
            }
        } catch (PDOException $e) {
            return ['error' => $e->getMessage(), 'token' => $token, 'fingerprinting' => $fingerprinting];
        }
        return false;
    }   
}
?>